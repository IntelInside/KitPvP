package com.multitimemc.kitpvp.listeners;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.multitimemc.kitpvp.Main;
import com.multitimemc.kitpvp.commands.Kits;

public class OnInvClick implements Listener {
	public Main plugin;
	
	public OnInvClick(Main main) {
		this.plugin = main;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if(e.getClickedInventory().getName() == Kits.kitMenu.getName()) {
			e.setCancelled(true);
			if(e.getCurrentItem().getType() == Material.BOW) {
				e.setCancelled(true);
				ItemStack bow = new ItemStack(Material.BOW, 1);
				ItemMeta bowMeta = bow.getItemMeta();
				bowMeta.addEnchant(Enchantment.ARROW_DAMAGE, 3, true);
				bowMeta.addEnchant(Enchantment.ARROW_FIRE, 1, false);
				bowMeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 2, true);
				bow.setItemMeta(bowMeta);
				
				ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
				ItemMeta swordMeta = sword.getItemMeta();
				swordMeta.addEnchant(Enchantment.KNOCKBACK, 1, false);
				sword.setItemMeta(swordMeta);
				
				ItemStack arrows = new ItemStack(Material.ARROW, 64);
				
				ItemStack steak = new ItemStack(Material.COOKED_BEEF, 64);
				
				ItemStack goldBoots = new ItemStack(Material.GOLD_BOOTS, 1);
				ItemStack leatherTunic = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
				ItemStack leatherLeggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
				ItemStack goldHelmet = new ItemStack(Material.GOLD_HELMET, 1);
				
				e.getWhoClicked().getInventory().addItem(bow);
				e.getWhoClicked().getInventory().addItem(sword);
				e.getWhoClicked().getInventory().addItem(arrows);
				e.getWhoClicked().getInventory().addItem(steak);
				e.getWhoClicked().getInventory().setBoots(goldBoots);
				e.getWhoClicked().getInventory().setChestplate(leatherTunic);
				e.getWhoClicked().getInventory().setLeggings(leatherLeggings);
				e.getWhoClicked().getInventory().setHelmet(goldHelmet);
				e.getWhoClicked().closeInventory();
				Main.kitActive.put(e.getWhoClicked().getName(), true);
			}
			if(e.getCurrentItem().getType() == Material.DIAMOND_CHESTPLATE) {
				ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
				
				ItemStack diamondBoots = new ItemStack(Material.DIAMOND_BOOTS, 1);
				ItemStack diamondChest = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
				ItemStack diamondLeggings = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
				ItemStack ironHelmet = new ItemStack(Material.IRON_HELMET, 1);
				
				ItemStack steak = new ItemStack(Material.COOKED_BEEF, 64);
				
				e.getWhoClicked().getInventory().addItem(sword);
				e.getWhoClicked().getInventory().addItem(steak);
				e.getWhoClicked().getInventory().setBoots(diamondBoots);
				e.getWhoClicked().getInventory().setChestplate(diamondChest);
				e.getWhoClicked().getInventory().setHelmet(ironHelmet);
				e.getWhoClicked().getInventory().setLeggings(diamondLeggings);
				e.getWhoClicked().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, -1, 2));
				Main.kitActive.put(e.getWhoClicked().getName(), true);
			}
			if(e.getCurrentItem().getType() == Material.POTION) {
				ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
				
				ItemStack goldChest = new ItemStack(Material.GOLD_CHESTPLATE, 1);
				ItemStack goldLeggings = new ItemStack(Material.GOLD_LEGGINGS, 1);
				ItemStack ironBoots = new ItemStack(Material.IRON_BOOTS, 1);
				ItemStack goldHelmet = new ItemStack(Material.GOLD_HELMET, 1);
				
				ItemStack bow = new ItemStack(Material.BOW, 1);
				ItemStack arrows = new ItemStack(Material.ARROW, 32);
				
				Potion potion = new Potion(PotionType.INSTANT_HEAL, 3);
				potion.setSplash(true);
				
				ItemStack healPotion = new ItemStack(Material.POTION, 1);
				potion.apply(healPotion);
				
				ItemStack steak = new ItemStack(Material.COOKED_BEEF, 64);
				
				ThrownPotion thrownPotion = e.getWhoClicked().launchProjectile(ThrownPotion.class);
				thrownPotion.setItem(healPotion);
				
				e.getWhoClicked().getInventory().setBoots(ironBoots);
				e.getWhoClicked().getInventory().setHelmet(goldHelmet);
				e.getWhoClicked().getInventory().setChestplate(goldChest);
				e.getWhoClicked().getInventory().setLeggings(goldLeggings);
				
				e.getWhoClicked().getInventory().addItem(sword);
				e.getWhoClicked().getInventory().addItem(bow);
				e.getWhoClicked().getInventory().addItem(arrows);
				e.getWhoClicked().getInventory().addItem(steak);
				for(int i = 0; i < 9; i++) {
					e.getWhoClicked().getInventory().addItem(healPotion);
				}
				
			}
			e.getWhoClicked().closeInventory();
		}
	}
}
