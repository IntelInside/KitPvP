package com.multitimemc.kitpvp.listeners;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.multitimemc.kitpvp.Main;
import com.multitimemc.kitpvp.SQLSetup;

public class OnDeath implements Listener {
	public Main plugin;
	
	public OnDeath(Main main) {
		this.plugin = main;
	}
	
	int Deaths;
	int Kills;
	
	boolean killerDoesExist;
	boolean victimDoesExist;
	
	public static void CreateRow(UUID UUID) throws SQLException {
		Statement statement = SQLSetup.SQL.getConnection().createStatement();
		statement.executeUpdate("INSERT IGNORE INTO `kp_stats` (UUID, KILLS, DEATHS) VALUES (UUID='" + UUID.toString() + "', KILLS='0', DEATHS='0');");
	}

	@EventHandler
	public void onDie(PlayerDeathEvent e) throws SQLException {
		Main.kitActive.remove(e.getEntity().getName());
		Main.kitActive.put(e.getEntity().getName(), false);
		Statement statement = SQLSetup.SQL.getConnection().createStatement();
		if(e.getEntityType().equals(EntityType.PLAYER)) {
			CreateRow(e.getEntity().getUniqueId());
			CreateRow(e.getEntity().getKiller().getUniqueId());
		}
		ResultSet drs = statement.executeQuery("SELECT DEATHS FROM `kp_stats` WHERE UUID='" + e.getEntity().getUniqueId().toString() + "';");
		while(drs.next()) {
			Deaths = drs.getInt("DEATHS");
			Deaths = Deaths + 1;
			statement.executeUpdate("UPDATE `kp_stats` SET DEATHS='" + Deaths + "' WHERE UUID='" + e.getEntity().getUniqueId().toString() + "';");
			drs.close();
			break;
		}
		ResultSet krs = statement.executeQuery("SELECT KILLS FROM `kp_stats` WHERE UUID='" + e.getEntity().getKiller().getUniqueId().toString() + "';"); // CAUTION: Using /kill WILL result in an NPE.
		while(krs.next()) {
			Kills = krs.getInt("KILLS");
			Kills = Kills + 1;
			statement.executeUpdate("UPDATE `kp_stats` SET KILLS='" + Kills + "' WHERE UUID='" + e.getEntity().getKiller().getUniqueId().toString() + "';");
			krs.close();
			break;
		}
	} 
}
