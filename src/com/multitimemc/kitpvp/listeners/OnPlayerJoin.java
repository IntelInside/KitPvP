package com.multitimemc.kitpvp.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.multitimemc.kitpvp.Main;

public class OnPlayerJoin implements Listener {
	public Main plugin;
	
	public OnPlayerJoin(Main main) {
		this.plugin = main;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		e.getPlayer().getInventory().clear();
		e.getPlayer().getInventory().setHelmet(null);
		e.getPlayer().getInventory().setChestplate(null);
		e.getPlayer().getInventory().setBoots(null);
		e.getPlayer().getInventory().setLeggings(null);
		Main.kitActive.put(e.getPlayer().getName(), false);
	}
}
