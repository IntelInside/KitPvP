package com.multitimemc.kitpvp.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.multitimemc.kitpvp.Main;

public class OnPlayerLeave implements Listener {
	public Main plugin;
	
	public OnPlayerLeave(Main main) {
		this.plugin = main;
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Main.kitActive.remove(e.getPlayer().getName());
	}
}
