package com.multitimemc.kitpvp.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import com.multitimemc.kitpvp.Main;

public class Kits implements CommandExecutor {
	
	public Main plugin;
	
	public Kits(Main main) {
		this.plugin = main;
	}
	
	public static Inventory kitMenu;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			if(Bukkit.getPlayer(sender.getName()).getWorld().getName().equalsIgnoreCase("kitpvp")) {
				if(Main.kitActive.get(sender.getName()).booleanValue() == false) {
					Player player = Bukkit.getPlayer(sender.getName());
					kitMenu = Bukkit.createInventory(null, 18, "Kits");
					
					ItemStack archer = new ItemStack(Material.BOW, 1);
					ItemMeta archerMeta = archer.getItemMeta();
					archerMeta.setDisplayName("Archer");
					List<String> archerLore = new ArrayList<String>();
					archerLore.add("Spawn with a super OP");
					archerLore.add("bow, but weak armor");
					archerLore.add("and a weak sword.");
					archerMeta.setLore(archerLore);
					archer.setItemMeta(archerMeta);
					
					ItemStack brute = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
					ItemMeta bruteMeta = brute.getItemMeta();
					bruteMeta.setDisplayName("Brute");
					List<String> bruteLore = new ArrayList<String>();
					bruteLore.add("Get some good armor,");
					bruteLore.add("and an iron sword,");
					bruteLore.add("but slowness two.");
					bruteMeta.setLore(bruteLore);
					brute.setItemMeta(bruteMeta);
					
					ItemStack healer = new ItemStack(Material.POTION, 1);
					Potion potion = new Potion(PotionType.INSTANT_HEAL);
					potion.setSplash(true);
					potion.apply(healer);
					ItemMeta healerMeta = healer.getItemMeta();
					healerMeta.setDisplayName("Healer");
					List<String> healerLore = new ArrayList<String>();
					healerLore.add("Get weak armor, sword,");
					healerLore.add("and bow, but lots of");
					healerLore.add("healing potions.");
					healerMeta.setLore(healerLore);
					healer.setItemMeta(healerMeta);
					
					kitMenu.setItem(5, healer);
					kitMenu.setItem(4, archer);
					kitMenu.setItem(3, brute);
					
					player.openInventory(kitMenu);
				} 
				else {
					sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "ERROR " + ChatColor.RESET + ChatColor.GRAY + ">> You currently have a kit selected.");
				}
			}
			
		}
		return false;
	}

}
