package com.multitimemc.kitpvp.commands;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.multitimemc.kitpvp.Main;
import com.multitimemc.kitpvp.SQLSetup;
import com.multitimemc.kitpvp.sqlcommands.StatsRetrieve;

public class Stats implements CommandExecutor {
	
	public Main plugin;

	public Stats(Main main) {
		this.plugin = main;
	}
	
	public static int kills;
	public static int deaths;
	
	public static void CreateRow(UUID UUID) throws SQLException {
		Statement statement = SQLSetup.SQL.getConnection().createStatement();
		statement.executeUpdate("INSERT IGNORE INTO `kp_stats` (UUID, KILLS, DEATHS) VALUES (UUID='" + UUID.toString() + "', KILLS='0', DEATHS='0');");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("stats")) {
			if(sender.hasPermission("kitpvp.stats")) {
				try {
					CreateRow(Bukkit.getPlayer(sender.getName()).getUniqueId());
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				try {
					StatsRetrieve.RetrieveDeaths(Bukkit.getPlayer(sender.getName()));
				} catch (SQLException e) {
					e.printStackTrace();
				}
				try {
					StatsRetrieve.RetrieveKills(Bukkit.getPlayer(sender.getName()));
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				
				sender.sendMessage(ChatColor.DARK_PURPLE + "Kills" + ChatColor.GRAY + "> " + ChatColor.DARK_PURPLE + kills);
				sender.sendMessage(ChatColor.DARK_PURPLE + "Deaths" + ChatColor.GRAY + "> " + ChatColor.DARK_PURPLE + deaths);
				
				
			} 
		}
		return false;
	}

}
