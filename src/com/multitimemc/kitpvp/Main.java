package com.multitimemc.kitpvp;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.multitimemc.kitpvp.commands.Kits;
import com.multitimemc.kitpvp.commands.Stats;
import com.multitimemc.kitpvp.listeners.OnDeath;
import com.multitimemc.kitpvp.listeners.OnInvClick;
import com.multitimemc.kitpvp.listeners.OnPlayerJoin;
import com.multitimemc.kitpvp.listeners.OnPlayerLeave;

public class Main extends JavaPlugin {
	public static Main plugin;
	public FileConfiguration config;
	public File file;
	protected String dbName;
	protected String dbUser;
	protected String dbPass;
	protected String dbPort;
	protected String dbAddress;
	
	public static HashMap<String, Boolean> kitActive = new HashMap<String, Boolean>();
	
	public Main() {
		
	}
	
	@Override
	public void onEnable() {
		config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		file = new File(getDataFolder(), "config.yml");
		dbName = config.getString("dbName");
		dbUser = config.getString("dbUser");
		dbPass = config.getString("dbPass");
		dbPort = config.getString("dbPort");
		dbAddress = config.getString("dbAddress");
		Bukkit.getLogger().info("Enabling KitPvP, developed by IntelInside.");
		this.getCommand("kits").setExecutor(new Kits(this));
		this.getCommand("stats").setExecutor(new Stats(this));
		Bukkit.getPluginManager().registerEvents(new OnDeath(this), this);
		Bukkit.getPluginManager().registerEvents(new OnPlayerJoin(this), this);
		Bukkit.getPluginManager().registerEvents(new OnPlayerLeave(this), this);
		Bukkit.getPluginManager().registerEvents(new OnInvClick(this), this);
		plugin = this;
		try {
			SQLSetup.Setup();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

	protected String getDbName() {
		return this.dbName;
	}

	protected String getDbUser() {
		return this.dbUser;
	}

	protected String getDbPass() {
		return this.dbPass;
	}

	protected String getDbPort() {
		return this.dbPort;
	}

	protected String getDbAddress() {
		return this.dbAddress;
	}
}
