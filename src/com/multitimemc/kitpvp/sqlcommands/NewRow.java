package com.multitimemc.kitpvp.sqlcommands;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import com.multitimemc.kitpvp.SQLSetup;

public class NewRow {
	public static void CreateRow(UUID UUID) throws SQLException {
		Statement statement = SQLSetup.SQL.getConnection().createStatement();
		statement.executeUpdate("INSERT IGNORE INTO `kp_stats` (UUID, KILLS, DEATHS) VALUES (UUID='" + UUID.toString() + "', KILLS='0', DEATHS='0');");
	}
}