package com.multitimemc.kitpvp.sqlcommands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.multitimemc.kitpvp.SQLSetup;
import com.multitimemc.kitpvp.commands.Stats;

public class StatsRetrieve {
	public static void RetrieveDeaths(Player player) throws SQLException {
		Statement statement = SQLSetup.SQL.getConnection().createStatement();
		ResultSet drs = statement.executeQuery("SELECT DEATHS FROM `kp_stats` WHERE UUID='" + Bukkit.getPlayer(player.getName()).getUniqueId() + "';");
		while(drs.next()) {
			Stats.deaths = drs.getInt("DEATHS");
			break;
		}
	}
	public static void RetrieveKills(Player player) throws SQLException {
		Statement statement = SQLSetup.SQL.getConnection().createStatement();
		ResultSet krs = statement.executeQuery("SELECT KILLS FROM `kp_stats` WHERE UUID='" + Bukkit.getPlayer(player.getName()).getUniqueId() + "';");
		while(krs.next()) {
			Stats.kills = krs.getInt("KILLS");
			break;
		}
	}
}
