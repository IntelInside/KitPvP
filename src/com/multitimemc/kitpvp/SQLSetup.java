package com.multitimemc.kitpvp;

import java.sql.SQLException;
import java.sql.Statement;

public class SQLSetup {
	public static MySQL SQL;
	
	public static void Setup() throws SQLException, ClassNotFoundException {
		SQL = new MySQL(Main.plugin.getDbAddress(), Main.plugin.getDbPort(), Main.plugin.getDbName(), Main.plugin.getDbUser(), Main.plugin.getDbPass());

		SQL.openConnection();
		
		Statement statement = SQL.getConnection().createStatement();
		statement.executeUpdate("CREATE TABLE IF NOT EXISTS `kp_stats` ( `UUID` TEXT NOT NULL , `KILLS` INT NOT NULL , `DEATHS` INT NOT NULL ) ENGINE = InnoDB;");

	}
}
